package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"fmt"
	"io"
	"math/big"
	"strings"

	"git.sr.ht/~sircmpwn/go-bare"
)

type Payload struct {
	Keys       [][]byte
	Secrets    []byte
	Will       []byte
	Polynomial [][]byte
}

func unmarshal(b []byte) (Payload, error) {
	bare.MaxArrayLength(30720)
	payload := Payload{
		Keys: [][]byte{},
	}
	buffer := bytes.NewBuffer(b)
	r := bare.NewReader(buffer)
	k, err := r.ReadUint()
	if err != nil {
		return payload, fmt.Errorf("while reading Keys length: %w", err)
	}
	var i uint64 = 0
	for ; i < k; i++ {
		l, err := r.ReadUint()
		if err != nil {
			return payload, fmt.Errorf("while reading Keys[%d] length: %w", i, err)
		}
		data := make([]byte, l)
		err = r.ReadDataFixed(data)
		if err != nil {
			return payload, fmt.Errorf("while reading Keys[%d]: %w", i, err)
		}
		payload.Keys = append(payload.Keys, data)
	}
	l, err := r.ReadUint()
	if err != nil {
		return payload, fmt.Errorf("while reading Secrets length: %w", err)
	}
	data := make([]byte, l)
	err = r.ReadDataFixed(data)
	if err != nil {
		return payload, fmt.Errorf("while reading Secrets: %w", err)
	}
	payload.Secrets = data
	l, err = r.ReadUint()
	if err != nil {
		return payload, fmt.Errorf("while reading Will length: %w", err)
	}
	data = make([]byte, l)
	err = r.ReadDataFixed(data)
	if err != nil {
		return payload, fmt.Errorf("while reading Will: %w", err)
	}
	payload.Will = data
	l, err = r.ReadUint()
	if err != nil {
		return payload, fmt.Errorf("while reading polynomial length: %w", err)
	}
	i = 0
	for ; i < l; i++ {
		ll, err := r.ReadUint()
		if err != nil {
			return payload, fmt.Errorf("while polynomial %d length: %w", i, err)
		}
		data := make([]byte, ll)
		err = r.ReadDataFixed(data)
		if err != nil {
			return payload, fmt.Errorf("while reading polynomial %d: %w", i, err)
		}
		payload.Polynomial = append(payload.Polynomial, data)
	}

	return payload, nil
}

func encrypt(plaintext, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, fmt.Errorf("while creating cipher: %w\n", err)
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, fmt.Errorf("while creating gcm: %w\n", err)
	}

	nonce := make([]byte, gcm.NonceSize())
	_, err = io.ReadFull(rand.Reader, nonce)
	if err != nil {
		return nil, fmt.Errorf("while creating nonce: %w\n", err)
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

func decrypt(ciphertext, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	if len(ciphertext) < gcm.NonceSize() {
		return nil, errors.New("malformed ciphertext")
	}

	return gcm.Open(nil,
		ciphertext[:gcm.NonceSize()],
		ciphertext[gcm.NonceSize():],
		nil,
	)
}

func toWords(bytes []byte, wordlist []string) []string {
	words := []string{}
	var z, r big.Int
	base := big.NewInt(2048)
	zero := big.NewInt(0)
	z.SetBytes(bytes) // note big-endian

	j := 0
	for z.Cmp(zero) > 0 {
		z.QuoRem(&z, base, &r)
		words = append(words, wordlist[r.Int64()])
		j++
	}
	return words
}

func fromWords(share string, wordlist map[string]int) ([]byte, error) {
	var word, exp, pos, z big.Int
	base := big.NewInt(2048)
	share = strings.ReplaceAll(share, ",", "")
	words := strings.Fields(share)
	for j := 0; j < len(words); j++ {
		w := words[j]
		index, present := wordlist[w]
		if !present {
			return nil, errors.New("no word " + w)
		}
		word.SetInt64(int64(index))
		exp.SetInt64(int64(j))
		pos.Exp(base, &exp, nil)
		word.Mul(&word, &pos)
		z.Add(&z, &word)
	}
	return z.Bytes(), nil
}
