all:V: decoder/joeblack.wasm encoder/joeblack
	cp decoder/joeblack.wasm .

init:V:
	cp shared/shared.go decoder/
	cp shared/wordlist decoder/
	cp shared/shared.go encoder/
	cp shared/wordlist encoder/

decoder/joeblack.wasm: init decoder/joeblack.go shared/shared.go
	cd decoder/ && mk

encoder/joeblack: init encoder/joeblack.go shared/shared.go
	cd encoder/ && mk

clean:V:
	rm -f decoder/joeblack.wasm decoder/shared.go decoder/wordlist
	rm -f encoder/joeblack encoder/shared.go encoder/wordlist
	rm -f encoder/secrets.dirty
