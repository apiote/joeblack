package main

import (
	"embed"
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"

	"github.com/codahale/sss"
	"syscall/js"
)

var (
	//go:embed wordlist
	wordlistFs embed.FS
	sharesNum  = 3
	wordlist   map[string]int
)

func main() {
	wordlistFile, err := wordlistFs.Open("wordlist")
	if err != nil {
		panic("while opening wordlist: " + err.Error())
	}
	defer wordlistFile.Close()
	wordlist = map[string]int{}
	word := ""
	for i := 0; i < 2048; i++ {
		fmt.Fscanf(wordlistFile, "%s", &word)
		wordlist[word] = i
	}
	js.Global().Set("decodeSSS", js.FuncOf(decodeSSS))
	js.Global().Set("decodePaperKey", js.FuncOf(decodePaperKey))
	js.Global().Set("decryptPayload", js.FuncOf(decryptPayload))
	<-make(chan bool)
}

// args:: shares :[sharesNum]string...
func decodeSSS(this js.Value, args []js.Value) any {
	if len(args) != sharesNum+1 {
		return fmt.Sprintf("ERR: Invalid number of arguments given %d, expected %d\n", len(args), sharesNum+1)
	}
	burntStrings := strings.Split(args[3].String(), ",")
	burnt := map[byte]struct{}{}
	for i, b := range burntStrings {
		burn := strings.TrimSpace(b)
		if burn == "" {
			continue
		}
		bUint, err := strconv.ParseUint(burn, 10, 8)
		if err != nil {
			return fmt.Sprintf("ERR: burnt[%d] (%s) malformed: %v\n", i, b, err)
		}
		burnt[byte(bUint)] = struct{}{}
	}

	shares := map[byte][]byte{}
	for i := 0; i < sharesNum; i++ {
		share := args[i].String()
		shareBytes, err := fromWords(share, wordlist)
		if err != nil {
			return fmt.Sprintf("ERR: while decoding words: %v", err)
		}
		shareNum := shareBytes[len(shareBytes)-1]
		shareBytes = shareBytes[:len(shareBytes)-1]
		if _, ok := burnt[shareNum]; ok {
			return fmt.Sprintf("ERR: udział nr %d jest spalony\n", shareNum)
		}
		shares[shareNum] = shareBytes
	}
	return base64.StdEncoding.EncodeToString(sss.Combine(shares))
}

// args:: words :string
func decodePaperKey(this js.Value, args []js.Value) any {
	if len(args) != 1 {
		return fmt.Sprintf("ERR: Invalid number of arguments given %d, expected 1\n", len(args))
	}
	share := args[0].String()
	shareBytes, err := fromWords(share, wordlist)
	if err != nil {
		return fmt.Sprintf("ERR: while decoding words: %v", err)
	}
	return base64.StdEncoding.EncodeToString(shareBytes)
}

// args:: wordsKey :base64, payload :base64[, isPaperKey :bool]
func decryptPayload(this js.Value, args []js.Value) any {
	if len(args) != 2 && len(args) != 3 {
		return fmt.Sprintf("ERR: Invalid number of arguments given %d, expected 2 or 3\n", len(args))
	}
	isPaperKey := false
	if len(args) == 3 {
		isPaperKey = args[2].Bool()
	}
	wordsKey, err := base64.StdEncoding.DecodeString(args[0].String())
	if err != nil {
		return fmt.Sprintf("ERR: while decoding base64 key: %v", err)
	}
	rawPayload, err := base64.StdEncoding.DecodeString(args[1].String())
	if err != nil {
		return fmt.Sprintf("ERR: while decoding base64 payload: %v", err)
	}
	payload, err := unmarshal(rawPayload)
	if err != nil {
		return fmt.Sprintf("ERR: while unmarshalling: %v", err)
	}
	var key []byte
	var keyNum int
	if isPaperKey {
		keyNum = 1
	} else {
		keyNum = 0
	}

	key, err = decrypt(payload.Keys[keyNum], wordsKey)
	if err != nil {
		return fmt.Sprintf("ERR: while decrypting key: %v", err)
	}
	secrets, err := decrypt(payload.Secrets, key)
	if err != nil {
		return fmt.Sprintf("ERR: while decrypting secrets: %v", err)
	}

	will, err := decrypt(payload.Will, key)
	if err != nil {
		return fmt.Sprintf("ERR: while decrypting will: %v", err)
	}
	willB64 := base64.StdEncoding.EncodeToString(will)

	return willB64 + "|" + string(secrets)
}
